use clap::{Command, Arg};
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug)]
struct Movie {
    id: i32,
    title: String,
    genre: String,
    year: i32,
}

fn main() {
    let matches = Command::new("movie_finder")
        .version("1.0.0")
        .author("Your Name")
        .about("Finds movies by year from a predefined dataset")
        .arg(
            Arg::new("year")
                .help("The year to find movies for")
                .required(true)
                .index(1),
        )
        .get_matches();

    let year: i32 = matches.value_of_t("year").expect("Year input is required and must be an integer");

    let movies = get_movies();
    let filtered_movies = filter_movies_by_year(&movies, year);

    if filtered_movies.is_empty() {
        println!("No movies found for the year {}", year);
    } else {
        println!("Movies found for the year {}:", year);
        for movie in filtered_movies {
            println!("{} - {} ({})", movie.id, movie.title, movie.genre);
        }
    }
}

fn get_movies() -> Vec<Movie> {
    vec![
        Movie { id: 1, title: "The Shawshank Redemption".to_string(), genre: "Drama".to_string(), year: 1994 },
        Movie { id: 2, title: "The Godfather".to_string(), genre: "Crime, Drama".to_string(), year: 1972 },
        Movie { id: 3, title: "The Dark Knight".to_string(), genre: "Action, Crime, Drama".to_string(), year: 2008 },
        Movie { id: 4, title: "The Godfather Part II".to_string(), genre: "Crime, Drama".to_string(), year: 1974 },
        Movie { id: 5, title: "12 Angry Men".to_string(), genre: "Crime, Drama".to_string(), year: 1957 },
        Movie { id: 6, title: "Schindler's List".to_string(), genre: "Biography, Drama, History".to_string(), year: 1993 },
        Movie { id: 7, title: "The Lord of the Rings: The Return of the King".to_string(), genre: "Adventure, Fantasy".to_string(), year: 2003 },
        Movie { id: 8, title: "Pulp Fiction".to_string(), genre: "Crime, Drama".to_string(), year: 1994 },
        Movie { id: 9, title: "The Good, the Bad and the Ugly".to_string(), genre: "Western".to_string(), year: 1966 },
        Movie { id: 10, title: "The Lord of the Rings: The Fellowship of the Ring".to_string(), genre: "Adventure, Fantasy".to_string(), year: 2001 },
        Movie { id: 11, title: "Fight Club".to_string(), genre: "Drama".to_string(), year: 1999 },
        Movie { id: 12, title: "Forrest Gump".to_string(), genre: "Drama, Romance".to_string(), year: 1994 },
        Movie { id: 13, title: "Inception".to_string(), genre: "Action, Adventure, Sci-Fi".to_string(), year: 2010 },
        Movie { id: 14, title: "The Lord of the Rings: The Two Towers".to_string(), genre: "Adventure, Fantasy".to_string(), year: 2002 },
        Movie { id: 15, title: "Star Wars: Episode V - The Empire Strikes Back".to_string(), genre: "Action, Adventure, Fantasy".to_string(), year: 1980 },
        Movie { id: 16, title: "The Matrix".to_string(), genre: "Action, Sci-Fi".to_string(), year: 1999 },
        Movie { id: 17, title: "Goodfellas".to_string(), genre: "Biography, Crime, Drama".to_string(), year: 1990 },
        Movie { id: 18, title: "One Flew Over the Cuckoo's Nest".to_string(), genre: "Drama".to_string(), year: 1975 },
        Movie { id: 19, title: "Seven Samurai".to_string(), genre: "Action, Adventure, Drama".to_string(), year: 1954 },
        Movie { id: 20, title: "Se7en".to_string(), genre: "Crime, Drama, Mystery".to_string(), year: 1995 },
        Movie { id: 21, title: "Life Is Beautiful".to_string(), genre: "Comedy, Drama, Romance".to_string(), year: 1997 },
        Movie { id: 22, title: "City of God".to_string(), genre: "Crime, Drama".to_string(), year: 2002 },
        Movie { id: 23, title: "The Silence of the Lambs".to_string(), genre: "Crime, Drama, Thriller".to_string(), year: 1991 },
        Movie { id: 24, title: "It's a Wonderful Life".to_string(), genre: "Drama, Family, Fantasy".to_string(), year: 1946 },
        Movie { id: 25, title: "Star Wars: Episode IV - A New Hope".to_string(), genre: "Action, Adventure, Fantasy".to_string(), year: 1977 },
        Movie { id: 26, title: "Saving Private Ryan".to_string(), genre: "Drama, War".to_string(), year: 1998 },
        Movie { id: 27, title: "Spirited Away".to_string(), genre: "Animation, Adventure, Family".to_string(), year: 2001 },
        Movie { id: 28, title: "The Green Mile".to_string(), genre: "Crime, Drama, Fantasy".to_string(), year: 1999 },
        Movie { id: 29, title: "Interstellar".to_string(), genre: "Adventure, Drama, Sci-Fi".to_string(), year: 2014 },
        Movie { id: 30, title: "Parasite".to_string(), genre: "Comedy, Drama, Thriller".to_string(), year: 2019 },
    ]
}


fn filter_movies_by_year(movies: &[Movie], year: i32) -> Vec<&Movie> {
    movies.iter().filter(|m| m.year == year).collect()
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_filter_movies_by_year() {
        let test_movies = vec![
            Movie { id: 1, title: "The Shawshank Redemption".to_string(), genre: "Drama".to_string(), year: 1994 },
            Movie { id: 2, title: "The Godfather".to_string(), genre: "Crime, Drama".to_string(), year: 1972 },
            Movie { id: 3, title: "The Dark Knight".to_string(), genre: "Action, Crime, Drama".to_string(), year: 2008 },
            Movie { id: 4, title: "Pulp Fiction".to_string(), genre: "Crime, Drama".to_string(), year: 1994 },
        ];

        // Test for year with multiple movies
        let filtered_movies_1994 = filter_movies_by_year(&test_movies, 1994);
        assert_eq!(filtered_movies_1994.len(), 2);
        assert!(filtered_movies_1994.iter().any(|&m| m.title == "The Shawshank Redemption"));
        assert!(filtered_movies_1994.iter().any(|&m| m.title == "Pulp Fiction"));

        // Test for year with no movies
        let filtered_movies_2020 = filter_movies_by_year(&test_movies, 2020);
        assert!(filtered_movies_2020.is_empty());

        // Test for year with one movie
        let filtered_movies_1972 = filter_movies_by_year(&test_movies, 1972);
        assert_eq!(filtered_movies_1972.len(), 1);
        assert_eq!(filtered_movies_1972[0].title, "The Godfather");
    }
}
