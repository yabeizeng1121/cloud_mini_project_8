# Movie Finder
Yabei Zeng

## Overview

Movie Finder is a Rust-based command-line tool that allows users to search for movies from a predefined dataset by year. This project showcases basic Rust programming concepts, including struct definitions, command-line argument parsing with `clap`, serialization with `serde`, and unit testing. The tool is designed to demonstrate data preprocessing/ingestion, testing, and the creation of a command-line interface in Rust.

### Features

- **Data Preprocessing/Ingestion**: The project comes with an embedded dataset of movies, each characterized by an ID, title, genre, and release year. This dataset serves as a simple example of data ingestion, where the data is directly embedded into the code.

- **Command Line Tool Functionality**: Utilizes `clap` for parsing command-line arguments, enabling users to specify the year they want to search for movies. The tool then filters the dataset for movies released in that year and displays the results in the terminal.

- **Testing**: Includes a test module to verify the functionality of the movie filtering logic. The tests ensure that the filtering process correctly identifies movies by year, handles years with no movies, and accurately processes years with multiple movies.

## Getting Started

### Prerequisites

- Rust and Cargo (Rust's package manager) must be installed on your machine. Installation instructions can be found at [https://www.rust-lang.org/tools/install](https://www.rust-lang.org/tools/install).

### Installation

1. Clone the repository to your local machine:

   ```sh
   git clone <path>
   ```

2. Navigate to the project directory:

    ```sh
    cd movie-finder
    ```
3. Build the project using Cargo:

    ```sh
    cargo build
    ```

## Usage
After building the project, you can run the Movie Finder tool by specifying a year as follows:

```sh
cargo run 1994
```
Replace 1994 with any year you wish to search for. The tool will then display all movies from the dataset that were released in that year.

## Testing

The Movie Finder project includes a set of test cases designed to ensure the reliability and correctness of the movie filtering functionality. Below is a detailed description of each test case implemented in the project:

### `test_filter_movies_by_year`

This test case verifies the core functionality of the Movie Finder tool, which is to filter movies by a specified year. It is divided into three scenarios:

1. **Multiple Movies in a Year**: Validates that the tool correctly identifies and returns all movies from a year when multiple movies are present. The year 1994 is used for this scenario, expecting "The Shawshank Redemption" and "Pulp Fiction" to be returned.

   - **Expected Outcome**: The function `filter_movies_by_year` returns exactly 2 movies for the year 1994, and the titles of these movies match the expected ones.

2. **No Movies in a Year**: Tests the tool's ability to handle years with no corresponding movies in the dataset. The year 2020 is chosen for this scenario, as it does not have any movies in the test dataset.

   - **Expected Outcome**: The function `filter_movies_by_year` returns an empty vector, indicating that no movies were found for the year 2020.

3. **Single Movie in a Year**: Checks if the tool can correctly filter out years with exactly one movie. The year 1972 is used here, with "The Godfather" being the only movie from that year in the dataset.

   - **Expected Outcome**: The function `filter_movies_by_year` returns a vector with a single movie for the year 1972, and the title of this movie is "The Godfather".

These test cases are crucial for ensuring that the Movie Finder tool operates as expected under various conditions. Running these tests helps to identify any issues with the movie filtering logic, ensuring that users receive accurate and reliable search results.

#  Result Preview

![Alt text](image.png)
![Alt text](image-1.png)


# References
- https://github.com/alfredodeza/rust-cli-example
- https://www.rust-lang.org/what/cli
- https://docs.rs/clap/latest/clap/